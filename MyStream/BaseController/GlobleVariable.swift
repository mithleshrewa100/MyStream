//
//  GlobleVariable.swift
//  SSUserApp
//
//  Created by MindCrew Technologies on 01/04/17.
//  Copyright © 2017 zombiedilip@gmail.com. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class GlobleVariable: NSObject {

    static let shared = GlobleVariable() //lazy init, and it only runs once

    //let Hel = "Helvatica"
    let formatServer : String = "MM/dd/yyyy HH:mm"
    
    //MARK:- Alert Message
    
    let noInternetMessage : String =            "No internet available."
    let strFont:String = "NunitoSans-Regular"
    let strBoldFont:String = "NunitoSans-SemiBold"
    
    let voxFont1 = UIFont.init(name: "vox_round", size: 20) //"vox_round"
    
    var fontItalic:UIFont = UIFont.init(name: "NunitoSans-LightItalic", size: 26)!
    var fontItalicMedium:UIFont = UIFont.init(name: "NunitoSans-LightItalic", size: 14)!

    var fontBoldMedium:UIFont = UIFont.init(name: "NunitoSans-SemiBold", size: 15)!

    var fontBoldsize:UIFont = UIFont.init(name: "NunitoSans-SemiBold", size: 16)!
    var fontBoldHeader:UIFont = UIFont.init(name: "NunitoSans-SemiBold", size: 20)!

    var fontRegular:UIFont = UIFont.init(name: "NunitoSans-Regular", size: 14)!

    var colorPrimaryTheme:UIColor = UIColor.white
    var colorGreen:UIColor = UIColor.init(red: 253/255.0, green: 169/255.0, blue: 44/255.0, alpha: 1)
    
    var colorDarkBlue:UIColor = UIColor.init(red: 2/255.0, green: 7/255.0, blue: 78/255.0, alpha: 1)
    var colorLightBlue:UIColor = UIColor.init(red: 65/255.0, green: 86/255.0, blue: 231/255.0, alpha: 1)
    var colorCell:UIColor = UIColor.init(red: 245/255.0, green: 248/255.0, blue: 251/255.0, alpha: 1)

    class func getLocalTime(_ strAppointmentDate: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = shared.formatServer
        let timeZone = NSTimeZone.local
        dateFormatter.timeZone = timeZone
        let appointmentDate: Date? = dateFormatter.date(from: strAppointmentDate)
        let currentTimeZone = NSTimeZone.local
        
        let utcTimeZone = NSTimeZone(name: "Europe/London")
        let currentGMTOffset: Int = currentTimeZone.secondsFromGMT(for: appointmentDate!)
        
//        open func secondsFromGMT(for aDate: Date) -> Int
        let gmtOffset: Int = (utcTimeZone?.secondsFromGMT)!

//        let gmtOffset: Int = utcTimeZone!.secondsFromGMT(forDate: appointmentDate)
        let gmtInterval: TimeInterval = TimeInterval(currentGMTOffset - gmtOffset)
        let destinationDate = Date(timeInterval: gmtInterval, since: appointmentDate!)
        
        let dateFormatters = DateFormatter()
        dateFormatters.dateFormat = shared.formatServer
        dateFormatters.timeZone = NSTimeZone.system
        let strLocalDate: String = dateFormatters.string(from: destinationDate)
        return strLocalDate
    }
    
    class func getServerLocalTime(_ strAppointmentDate: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy HH:mm:ss"
        let timeZone = NSTimeZone.local
        dateFormatter.timeZone = timeZone
        var appointmentDate: Date? = dateFormatter.date(from: strAppointmentDate)
        if(appointmentDate == nil) {
            dateFormatter.dateFormat = "MM/dd/yyyy HH:mm"
            appointmentDate = dateFormatter.date(from: strAppointmentDate)
            
            if(appointmentDate == nil) {
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                appointmentDate = dateFormatter.date(from: strAppointmentDate)
                
            }
        }
        let currentTimeZone = NSTimeZone.local
        
        let utcTimeZone = NSTimeZone(name: "Europe/London")
        let currentGMTOffset: Int = currentTimeZone.secondsFromGMT(for: appointmentDate!)
        
        let gmtOffset: Int = (utcTimeZone?.secondsFromGMT)!

        let gmtInterval: TimeInterval = TimeInterval(currentGMTOffset - gmtOffset)
        let destinationDate = Date(timeInterval: gmtInterval, since: appointmentDate!)
        
        let dateFormatters = DateFormatter()
        dateFormatters.dateFormat = "MM/dd/yyyy HH:mm:ss"//shared.formatServer
        dateFormatters.timeZone = NSTimeZone.system
        let strLocalDate: String = dateFormatters.string(from: destinationDate)
        return strLocalDate
    }
}

extension String {
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
