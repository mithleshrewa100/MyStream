//
//  Extensionfile.swift
//  PassDamicApp
//
//  Created by Mithilesh on 19/02/19.
//  Copyright © 2019 Mithilesh Satnami. All rights reserved.
//

import Foundation
import UIKit

extension UIView
{
    func GradiantColor()
    {
        let gradientLayer:CAGradientLayer = CAGradientLayer()
        gradientLayer.frame.size = self.frame.size
        gradientLayer.colors =
            [UIColor.init(red: 224/255.0, green: 130/255.0, blue: 30/255.0, alpha: 1),UIColor.init(red: 224/255.0, green: 130/255.0, blue: 30/255.0, alpha: 1).cgColor]
        //Use diffrent colors
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
        self.layer.addSublayer(gradientLayer)
    }
    
}
//extension UIViewController{
//    func showHud(_ message: String) {
//        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
//        hud.label.text = message
//        hud.isUserInteractionEnabled = false
//        _ = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.hideHUD), userInfo: nil, repeats: true)
//    }
//   @objc func hideHUD() {
//        MBProgressHUD.hide(for: self.view, animated: true)
////        self.dismiss(animated: true, completion: nil)
//    }
//}

class  ClassindicatoreView:UIViewController
{
    var IndicatorView:UIView = UIView()
    var activityindicator:UIActivityIndicatorView = UIActivityIndicatorView()

    func LoaderStart() {
        IndicatorView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        IndicatorView.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.view.addSubview(IndicatorView)
        
        let viewloader:UIView = UIView()
        viewloader.frame = CGRect(x:(UIScreen.main.bounds.size.width-50)/2.0, y:(UIScreen.main.bounds.size.height-50)/2.0, width:50, height:50)
        viewloader.backgroundColor = UIColor.lightGray
        viewloader.layer.cornerRadius = 5.0
        IndicatorView.addSubview(viewloader)
        
        activityindicator.frame = CGRect(x: 0, y: 0, width: 46, height: 46)
        activityindicator.hidesWhenStopped = false
        activityindicator.style = UIActivityIndicatorView.Style.gray
        activityindicator.color = UIColor.red
        activityindicator.startAnimating()
        viewloader.addSubview(activityindicator)
    }
    
    func stopIndicatore(){
        activityindicator.stopAnimating()
        IndicatorView.removeFromSuperview()
    }
}

extension UIView
{
    func setGradientBackground() {
        let colorTop =  UIColor.init(red: 238/255.0, green: 238/255.0, blue: 125/255.0, alpha: 1).cgColor
        let colorBottom = UIColor.init(red: 254/255.0, green: 253/255.0, blue: 247/255.0, alpha: 1).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame.size = self.frame.size
        self.layer.addSublayer(gradientLayer)
    }
}
extension UIView {
    
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: -1, height: 1)
        layer.shadowRadius = 1
        
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}

extension UIView {
    
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        clipsToBounds = true
        layer.shadowRadius = radius
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}

extension UILabel
{
    func applyGradientWith(startColor: UIColor, endColor: UIColor) -> Bool {
        
        return true
    }
    
}

extension CGFloat
{
func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
    let label:UILabel = UILabel(frame: CGRect(x:0, y:0, width:width, height: CGFloat.greatestFiniteMagnitude))
    label.numberOfLines = 0
    label.lineBreakMode = NSLineBreakMode.byWordWrapping
    label.font = font
    label.text = text
    
    label.sizeToFit()
    return label.frame.height
}
}

extension UIImageView
{
    
    func partialGradient(forViewSize size: CGSize, proportion p: CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        
        let context = UIGraphicsGetCurrentContext()
        
        
        context?.setFillColor(UIColor.darkGray.cgColor)
        context?.fill(CGRect(origin: .zero, size: size))
        
        let c1 = UIColor.orange.cgColor
        let c2 = UIColor.red.cgColor
        
        let top = CGPoint(x: 0, y: size.height * (1.0 - p))
        let bottom = CGPoint(x: 0, y: size.height)
        
        let colorspace = CGColorSpaceCreateDeviceRGB()
        
        if let gradient = CGGradient(colorsSpace: colorspace, colors: [c1, c2] as CFArray, locations: [0.0, 1.0]){
            // change 0.0 above to 1-p if you want the top of the gradient orange
            context?.drawLinearGradient(gradient, start: top, end: bottom, options: CGGradientDrawingOptions.drawsAfterEndLocation)
        }
        
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
}


extension UIView {
    func Shaplayer(_ shap:CAShapeLayer, corner:CGFloat)
    {
        shap.bounds = self.frame
        shap.position = self.center
        shap.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.bottomRight , .bottomLeft , .topRight , .topLeft], cornerRadii: CGSize(width: corner, height: corner)).cgPath
//        self.layer.backgroundColor = UIColor.init(red: 47/255.0, green: 107/255.0, blue: 193/255.0, alpha: 1).cgColor
        self.layer.mask = shap
    }
    
    func ShaplayerLeft(_ shap:CAShapeLayer, corner:CGFloat)
    {
        shap.bounds = self.frame
        shap.position = self.center
        shap.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topLeft , .bottomLeft], cornerRadii: CGSize(width: corner, height: corner)).cgPath
        self.layer.mask = shap
    }
    func ShaplayerTopRight(_ shap:CAShapeLayer, corner:CGFloat)
    {
        shap.bounds = self.frame
        shap.position = self.center
        shap.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topRight], cornerRadii: CGSize(width: corner, height: corner)).cgPath
        self.layer.mask = shap
    }
    func ShaplayerUp(_ shap:CAShapeLayer, corner:CGFloat)
    {
        shap.bounds = self.frame
        shap.position = self.center
        shap.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topRight , .topLeft], cornerRadii: CGSize(width: corner, height: corner)).cgPath
        self.layer.mask = shap
    }
    func ShaplayerDown(_ shap:CAShapeLayer, corner:CGFloat)
    {
        shap.bounds = self.frame
        shap.position = self.center
        shap.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.bottomLeft, .bottomRight], cornerRadii: CGSize(width: corner, height: corner)).cgPath
        self.layer.mask = shap
    }

    func ShaplayerBotomRight(_ shap:CAShapeLayer, corner:CGFloat)
    {
        shap.bounds = self.frame
        shap.position = self.center
        shap.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [ .bottomRight], cornerRadii: CGSize(width: corner, height: corner)).cgPath
        self.layer.mask = shap
    }

    func ShaplayerViewTicket(_ shap:CAShapeLayer, corner:CGFloat)
    {
        shap.bounds = self.frame
        shap.position = self.center
        shap.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topLeft , .bottomLeft , .bottomRight, .topRight], cornerRadii: CGSize(width: corner, height: corner)).cgPath
        self.layer.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1).cgColor
        self.layer.mask = shap
    }
    func ShaplayerRight(_ shap:CAShapeLayer, corner:CGFloat)
    {
        shap.bounds = self.frame
        shap.position = self.center
        shap.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topLeft , .bottomLeft , .bottomRight, .topRight], cornerRadii: CGSize(width: corner, height: corner)).cgPath
        self.layer.mask = shap
    }

    
}
//WEBservice

extension String
{
    func append(_ other: String,_appends: String)-> String
    {
        let str:String = other+_appends
        return str
    }
}

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}

extension NSMutableData {
    func appendString(string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}

public extension String {
    
    //right is the first encountered string after left
    func between(_ left: String, _ right: String) -> String? {
        guard
            let leftRange = range(of: left), let rightRange = range(of: right, options: .backwards)
            , leftRange.upperBound <= rightRange.lowerBound
            else { return nil }
        
        let sub = self[leftRange.upperBound...]
        let closestToLeftRange = sub.range(of: right)!
        return String(sub[..<closestToLeftRange.lowerBound])
    }
    
    var length: Int {
        get {
            return self.count
        }
    }
    
    func substring(to : Int) -> String {
        let toIndex = self.index(self.startIndex, offsetBy: to)
        return String(self[...toIndex])
    }
    
    func substring(from : Int) -> String {
        let fromIndex = self.index(self.startIndex, offsetBy: from)
        return String(self[fromIndex...])
    }
    
    func substring(_ r: Range<Int>) -> String {
        let fromIndex = self.index(self.startIndex, offsetBy: r.lowerBound)
        let toIndex = self.index(self.startIndex, offsetBy: r.upperBound)
        let indexRange = Range<String.Index>(uncheckedBounds: (lower: fromIndex, upper: toIndex))
        return String(self[indexRange])
    }
    
    func character(_ at: Int) -> Character {
        return self[self.index(self.startIndex, offsetBy: at)]
    }
    
    func lastIndexOfCharacter(_ c: Character) -> Int? {
        return range(of: String(c), options: .backwards)?.lowerBound.encodedOffset
    }
}
//MARK:- Replace Space
extension String {
    func replace(string:String, replacement:String) -> String {
        return self.replacingOccurrences(of: string, with: replacement, options: NSString.CompareOptions.literal, range: nil)
    }
    
    func removeWhitespace() -> String {
        return self.replace(string: " ", replacement: "")
    }
}


class LeftAlignedCollectionViewFlowLayout: UICollectionViewFlowLayout {

    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let attributes = super.layoutAttributesForElements(in: rect)

        var leftMargin = sectionInset.left
        var maxY: CGFloat = -1.0
        attributes?.forEach { layoutAttribute in
            if layoutAttribute.frame.origin.y >= maxY {
                leftMargin = sectionInset.left
            }

            layoutAttribute.frame.origin.x = leftMargin

            leftMargin += layoutAttribute.frame.width + minimumInteritemSpacing
            maxY = max(layoutAttribute.frame.maxY , maxY)
        }

        return attributes
    }
}
